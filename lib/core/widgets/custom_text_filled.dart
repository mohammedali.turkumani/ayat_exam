import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';

import '../strings/assets.dart';
import '../utils/colors.dart';
import '../utils/custom_text_style.dart';

class CustomTextField extends StatefulWidget {
  final String? hintText;
  final TextEditingController controller;
  final TextInputType keyboardType;
  final FocusNode? focusNode;
  final bool? removeBorder;
  final bool? validate;
  final bool? isAuth;
  final bool? isEmpty;
  final bool? isUserName;

  const CustomTextField({
    Key? key,
    this.hintText = '',
    required this.controller,
    this.isAuth = true,
    this.validate = false,
    this.keyboardType = TextInputType.text,
    this.removeBorder = false,
    this.focusNode,
    this.isEmpty = false,
    this.isUserName = true,
  }) : super(key: key);

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool showPassword = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.1),
          spreadRadius: 3,
          blurRadius: 12,
          offset: const Offset(0, 0), // changes position of shadow
        ),
      ]),
      child: TextFormField(
        controller: widget.controller,
        keyboardType: widget.keyboardType,
        obscureText: !showPassword && !widget.isUserName!,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        cursorColor: Colors.grey,
        style: TextStyleApp.black20TextFormFilled,
        decoration: InputDecoration(
          isDense: true,
          fillColor: Colors.white,
          filled: true,
          border: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.grey),
            borderRadius: BorderRadius.circular(20.r),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColors.originBlue),
            borderRadius: BorderRadius.circular(20.r),
          ),
          prefixIcon: Padding(
            padding: EdgeInsets.all(15.r),
            child: SvgPicture.asset(
              widget.isUserName! ? Assets.userNameIcon : Assets.passwordIcon,
              width: 10,
              height: 10,
              color: AppColors.originBlue,
            ),
          ),
          suffixIcon: !widget.isUserName!
              ? GestureDetector(
                  onTap: () {
                    setState(() {
                      showPassword = !showPassword;
                    });
                  },
                  child: Padding(
                    padding: EdgeInsets.all(15.r),
                    child: SvgPicture.asset(
                      showPassword
                          ? Assets.showPasswordIcon
                          : Assets.hidePasswordIcon,
                      width: 10,
                      height: 10,
                      color: AppColors.originBlue,
                    ),
                  ),
                )
              : null,
          hintStyle: TextStyleApp.grey16,
          hintText: widget.hintText,
          errorStyle: TextStyleApp.errorStyle,
          enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Colors.grey.withOpacity(0.4), width: 1.0),
            borderRadius: BorderRadius.circular(20.r),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.red),
            borderRadius: BorderRadius.circular(20.r),
          ),
        ),
        onChanged: (value) {},
        validator: widget.validate!
            ? (widget.isEmpty!
                ? null
                : (value) {
                    if (value != null && value.trim().isEmpty) {
                      return "requiredField";
                    }
                    return null;
                  })
            : null,
      ),
    );
  }
}
