import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../utils/colors.dart';
import '../utils/custom_text_style.dart';

class CustomFilledButton extends StatelessWidget {
  final void Function()? onClick;
  final String text;
  final Color color;
  final bool isOutlined;
  final double heightSize;
  final double radiusSize;
  final double widthSize;
  final bool isBlackBorder;
  final bool? isLoading;

  const CustomFilledButton({
    Key? key,
    this.onClick,
    required this.text,
    this.color = AppColors.originBlue,
    this.isOutlined = false,
    this.heightSize = 0.0,
    this.widthSize = 0.0,
    this.isBlackBorder = false,
    this.isLoading = false,
    this.radiusSize = 0.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: heightSize == 0.0 ? 60.h : heightSize,
      width: widthSize == 0.0 ? 0.4.sw : widthSize,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: onClick == null
              ? Colors.white
              : isOutlined
                  ? Colors.white
                  : color,
          side: BorderSide(
            color: onClick == null
                ? Colors.grey
                : isOutlined
                    ? isBlackBorder
                        ? Colors.black
                        : Colors.grey
                    : Colors.grey,
          ),
          shape: RoundedRectangleBorder(
            borderRadius:
                BorderRadius.circular(radiusSize == 0.0 ? 15.r : radiusSize),
          ),
        ),
        onPressed: isLoading! ? null : onClick,
        child: isLoading!
            ? Transform.scale(
                scale: 0.6,
                child: const Center(
                  child: CircularProgressIndicator(
                    color: AppColors.originBlue,
                  ),
                ),
              )
            : Text(
                text,
                style: onClick == null
                    ? TextStyleApp.lightGrey18
                    : isOutlined
                        ? isBlackBorder
                            ? TextStyleApp.black18Bold
                            : TextStyleApp.grey25
                        : TextStyleApp.white18,
              ),
      ),
    );
  }
}
