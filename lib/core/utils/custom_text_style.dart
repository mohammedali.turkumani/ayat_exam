import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'colors.dart';

class TextStyleApp {
  ///--------------------------------------------------------------------
  ///  Black Text

  static TextStyle black20 = TextStyle(
    fontSize: 20.sp,
    color: Colors.black,
  );

  static TextStyle black25 = TextStyle(
    fontSize: 25.sp,
    color: Colors.black,
  );
  static TextStyle black20TextFormFilled = TextStyle(
    fontSize: 20.sp,
    color: Colors.black,
    height: 1.5,
  );

  static TextStyle black18Bold = TextStyle(
    fontSize: 18.sp,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  static TextStyle black16 = TextStyle(
    fontSize: 16.sp,
    color: Colors.black,
    fontWeight: FontWeight.w500,
  );
  static TextStyle black12 = TextStyle(
    fontSize: 12.sp,
    color: Colors.black,
    fontWeight: FontWeight.w500,
  );
  static TextStyle black10 = TextStyle(
    fontSize: 10.sp,
    color: Colors.black,
    fontWeight: FontWeight.w500,
  );
  static TextStyle black14 = TextStyle(
    fontSize: 14.sp,
    color: Colors.black,
  );

  static TextStyle black20Bold = TextStyle(
    fontSize: 20.sp,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );
  static TextStyle lightBlack14Bold = TextStyle(
    fontSize: 14.sp,
    color: Colors.black87,
    fontWeight: FontWeight.w800,
  );

  static TextStyle lightBlack20Bold = TextStyle(
    fontSize: 20.sp,
    color: Colors.black87,
    fontWeight: FontWeight.w800,
  );

  static TextStyle lightBlack25Bold = TextStyle(
    fontSize: 25.sp,
    color: Colors.black87,
    fontWeight: FontWeight.w800,
  );
  static TextStyle black25Bold = TextStyle(
    fontSize: 25.sp,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );
  static TextStyle black32Bold = TextStyle(
    fontSize: 32.sp,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  static TextStyle black16Bold = TextStyle(
    fontSize: 16.sp,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  static TextStyle black16UnderLine = TextStyle(
    fontSize: 16.sp,
    color: Colors.black,
    decoration: TextDecoration.underline,
  );

  static TextStyle black10UnderLine = TextStyle(
    fontSize: 10.sp,
    color: Colors.black,
    decoration: TextDecoration.underline,
  );

  static TextStyle black14Bold = TextStyle(
    fontSize: 14.sp,
    color: Colors.black,
    fontWeight: FontWeight.bold,
  );

  ///--------------------------------------------------------------------
  ///  Grey Text

  static TextStyle grey25 = TextStyle(
    fontSize: 25.sp,
    color: Colors.grey,
  );

  static TextStyle grey14 = TextStyle(
    fontSize: 14.sp,
    color: Colors.grey,
  );

  static TextStyle lightGrey14 = TextStyle(
    fontSize: 14.sp,
    color: Colors.grey,
  );

  static TextStyle lightGrey12 = TextStyle(
    fontSize: 12.sp,
    color: Colors.grey,
  );

  static TextStyle lightGrey14Line = TextStyle(
      fontSize: 14.sp,
      color: Colors.grey,
      decoration: TextDecoration.underline);

  static TextStyle lightGrey18 = TextStyle(
    fontSize: 18.sp,
    color: Colors.grey,
  );
  static TextStyle lightGrey16 = TextStyle(
    fontSize: 16.sp,
    color: Colors.grey,
  );

  static TextStyle grey16 = TextStyle(
    fontSize: 16.sp,
    color: Colors.grey,
  );
  static TextStyle grey18 = TextStyle(
    fontSize: 18.sp,
    color: Colors.grey,
  );

  static TextStyle grey18Bold = TextStyle(
    fontSize: 18.sp,
    color: Colors.grey,
    fontWeight: FontWeight.bold,
  );

  static TextStyle grey20 = TextStyle(
    fontSize: 20.sp,
    color: Colors.grey,
  );

  ///--------------------------------------------------------------------
  ///  White Text

  static TextStyle white18 = TextStyle(
    fontSize: 18.sp,
    color: Colors.white,
  );

  static TextStyle white28 = TextStyle(
    fontSize: 28.sp,
    color: Colors.white,
  );
  static TextStyle white24Bold = TextStyle(
    fontSize: 24.sp,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );
  static TextStyle white22UnderLine = TextStyle(
    fontSize: 22.sp,
    color: Colors.white,
    decoration: TextDecoration.underline,
    decorationColor: Colors.white,
  );

  static TextStyle white50Bold = TextStyle(
    fontSize: 50.sp,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static TextStyle white16 = TextStyle(
    fontSize: 16.sp,
    color: Colors.white,
  );

  static TextStyle white14 = TextStyle(
    fontSize: 14.sp,
    color: Colors.white,
  );
  static TextStyle white12 = TextStyle(
    fontSize: 12.sp,
    color: Colors.white,
  );

  static TextStyle white16Bold = TextStyle(
    fontSize: 16.sp,
    color: Colors.white,
    fontWeight: FontWeight.bold,
  );

  static TextStyle white14SemiBold = TextStyle(
    fontSize: 14.sp,
    color: Colors.white,
    fontWeight: FontWeight.w700,
  );

  ///--------------------------------------------------------------------
  ///  Red Text

  static TextStyle red18Bold = TextStyle(
    fontSize: 18.sp,
    color: Colors.red,
    fontWeight: FontWeight.bold,
  );

  static TextStyle red20Bold = TextStyle(
    fontSize: 20.sp,
    color: Colors.red,
    fontWeight: FontWeight.bold,
  );

  static TextStyle errorStyle = TextStyle(
    fontSize: 12.sp,
    height: 0.08.h,
    color: Colors.red,
  );

  ///--------------------------------------------------------------------
  ///  blue Text

  static TextStyle blue18Bold = TextStyle(
    fontSize: 18.sp,
    color: AppColors.originBlue,
    fontWeight: FontWeight.bold,
  );

  static TextStyle blue20Bold = TextStyle(
    fontSize: 20.sp,
    color: AppColors.originBlue,
    fontWeight: FontWeight.bold,
  );

  static TextStyle blue24 = TextStyle(
    fontSize: 24.sp,
    color: AppColors.originBlue,
  );

  static TextStyle blue30 = TextStyle(
    fontSize: 30.sp,
    color: AppColors.originBlue,
  );

  static TextStyle blue22UnderLine = TextStyle(
    fontSize: 22.sp,
    color: AppColors.originBlue,
    decoration: TextDecoration.underline,
    decorationColor: AppColors.originBlue,
  );

  static TextStyle blue12 = TextStyle(
    fontSize: 12.sp,
    color: AppColors.originBlue,
  );
  static TextStyle blue16 = TextStyle(
    fontSize: 16.sp,
    color: AppColors.originBlue,
  );
}
