import '../../features/services/domain/entities/medical_service.dart';

bool hasCheckedSubService(MedicalService medicalService) {
  return medicalService.subService.any((subService) => subService.isChecked!);
}
