import 'package:flutter/material.dart';

import 'custom_text_style.dart';

class SnackBarMessage {
  void showSuccessSnackBar({
    required String message,
    required BuildContext context,
  }) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: const Duration(milliseconds: 1000),
          elevation: 6.0,
          behavior: SnackBarBehavior.floating,
          content: Text(
            message,
            style: TextStyleApp.white18,
          ),
          backgroundColor: Colors.green,
        ),
      );
    });
  }

  void showWarningSnackBar(
      {required String message, required BuildContext context}) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          elevation: 6.0,
          duration: const Duration(milliseconds: 1000),
          behavior: SnackBarBehavior.floating,
          content: Text(
            message,
            style: TextStyleApp.white18,
          ),
          backgroundColor: Colors.black,
        ),
      );
    });
  }

  void showErrorSnackBar(
      {required String message, required BuildContext context}) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          elevation: 6.0,
          duration: const Duration(milliseconds: 1000),
          behavior: SnackBarBehavior.floating,
          content: Text(
            message,
            style: TextStyleApp.white18,
          ),
          backgroundColor: Colors.red,
        ),
      );
    });
  }
}
