import 'package:flutter/material.dart';

void navigateTo(context, widget) {
  Navigator.push(
    context,
    slideTransition(widget),
  );
}

void navigateAndFinish(context, widget) {
  Navigator.pushAndRemoveUntil(
    context,
    slideTransition(widget),
    (Route<dynamic> route) => false,
  );
}

void navigateAndReplacement(context, widget) {
  Navigator.pushReplacement(
    context,
    slideTransition(widget),
  );
}

Route slideTransition(Widget page) {
  return PageRouteBuilder(
    transitionDuration: const Duration(milliseconds: 250),
    pageBuilder: (context, animation, secondaryAnimation) => page,
    transitionsBuilder: (context, animation, secondaryAnimation, page) {
      const begin = Offset(1.0, 0.0);
      const end = Offset.zero;
      const curve = Curves.linear;
      var slide = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(slide),
        child: page,
      );
    },
  );
}
