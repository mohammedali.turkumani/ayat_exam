import 'dart:ui';

import 'package:easy_localization/easy_localization.dart';

class CustomDate {
  static String getDayName(DateTime date, Locale locale) {
    return DateFormat('EEEE', locale.languageCode).format(date);
  }

  static String getDate(DateTime? date) {
    if (date == null) {
      return "";
    }
    return DateFormat('d/MM/yyyy').format(date);
  }

  static String getTimeFromDateAndTime(DateTime date) {
    return DateFormat.jm().format(date);
  }
}
