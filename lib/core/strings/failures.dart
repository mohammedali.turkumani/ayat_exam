import '../error/failure.dart';

const String serverFailureMessage = 'حدث خطأ ما الرجاء المحاولة مرة أخرى';
const String globalFailureMessage = 'حدث خطأ ما';
const String userFailureMessage = 'الرجاء تعبئة الحقول كافة';
const String emptyCacheFailureMessage = 'لا يوجد بيانات';
const String offlineFailureMessage =
    'الرجاء التأكد من اتصالك بالانترنت والمحاولة مرة أخرى';
const String notAuthCacheFailureMessage = 'الرجاء تسجيل الدخول';
const String errorAuthFailureMessage = "خطأ في اسم المستخدم أو كلمة المرور";
const String errorPasswordMatchFailureMessage =
    "الرجاء التأكد من تطابق كلمة السر";

String mapFailureToMessage(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return serverFailureMessage;
    case EmptyCacheFailure:
      return emptyCacheFailureMessage;
    case OfflineFailure:
      return offlineFailureMessage;
    case NotAuthCacheFailure:
      return notAuthCacheFailureMessage;
    case ErrorAuthFailure:
      return errorAuthFailureMessage;
    case GlobalFailure:
      return globalFailureMessage;

    default:
      return serverFailureMessage;
  }
}
