class Assets {
  Assets();
  static String domeLogo = "assets/svg/domecare_logo.svg";
  static String userNameIcon = "assets/svg/ic_username.svg";
  static String passwordIcon = "assets/svg/ic_password.svg";
  static String showPasswordIcon = "assets/svg/ic_view_password.svg";
  static String hidePasswordIcon = "assets/svg/ic_show_password.svg";
}
