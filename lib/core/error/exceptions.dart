class ServerException implements Exception {}

class EmptyCacheException implements Exception {}

class GlobalErrorException implements Exception {}

class OfflineException implements Exception {}

class NotAuthException implements Exception {}

class ErrorAuthException implements Exception {}
