import 'package:ayat_test/features/services/domain/usecases/get_serives_user_case.dart';
import 'package:ayat_test/features/services/presentation/bloc/services_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/network/network_info.dart';
import 'features/auth/data/datasources/remote_data_source.dart';
import 'features/auth/data/repositories/auth_repository_imp.dart';
import 'features/auth/domain/repositories/auth_repository.dart';
import 'features/auth/domain/usecases/sign_in.dart';
import 'features/auth/presentation/bloc/auth_bloc.dart';
import 'features/services/data/datasources/remote_data_source.dart';
import 'features/services/data/repositories/medical_services_repository_impl.dart';
import 'features/services/domain/repositories/medical_services_repository.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //! Features - Auth

  // Bloc
  sl.registerFactory(
    () => AuthBloc(
      signIn: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => SignInUseCase(repository: sl()));

  // Repository

  sl.registerLazySingleton<AuthRepository>(
      () => AuthRepositoryImpl(remoteDataSource: sl(), networkInfo: sl()));

  // Data sources
  sl.registerLazySingleton<AuthRemoteDataSource>(
      () => AuthRemoteDataSourceImp(client: sl()));

  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  //! External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => InternetConnectionChecker());

  //! Features - Services

  // Bloc
  sl.registerFactory(
    () => ServicesBloc(
      getMedicalServicesUseCase: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => GetMedicalServicesUseCase(repository: sl()));

  // Repository

  sl.registerLazySingleton<MedicalServicesRepository>(() =>
      MedicalServicesRepositoryImpl(
          networkInfo: sl(), servicesRemoteDataSource: sl()));

  // Data sources
  sl.registerLazySingleton<ServicesRemoteDataSource>(
      () => ServicesRemoteDataSourceImp());
}
