import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../../../core/error/exceptions.dart';

abstract class AuthRemoteDataSource {
  Future<Unit> signIn(String phoneOrUsername, String password);
}

class AuthRemoteDataSourceImp implements AuthRemoteDataSource {
  final http.Client client;

  AuthRemoteDataSourceImp({required this.client});

  @override
  Future<Unit> signIn(String phoneOrUsername, String password) async {
    Map<String, String> body = {
      "username": phoneOrUsername,
      "password": password,
    };
    print(body);
    final response = await client.post(
      Uri.parse("https://dummyjson.com/auth/login"),
      body: json.encode(body),
      headers: {
        "Content-Type": "application/json",
      },
    );

    if (response.statusCode == 200) {
      var decodeJson = json.decode(response.body);
      final token = decodeJson['token'];
      final sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString("TOKEN", token);
      return Future.value(unit);
    } else if (response.statusCode == 400) {
      throw ErrorAuthException();
    } else {
      throw ServerException();
    }
  }
}
