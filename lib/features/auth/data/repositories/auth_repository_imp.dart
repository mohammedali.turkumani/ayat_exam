import 'package:dartz/dartz.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failure.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/repositories/auth_repository.dart';
import '../datasources/remote_data_source.dart';

class AuthRepositoryImpl implements AuthRepository {
  final AuthRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  AuthRepositoryImpl(
      {required this.remoteDataSource, required this.networkInfo});
  @override
  Future<Either<Failure, Unit>> signIn(
      String phoneOrUsername, String password) async {
    if (await networkInfo.isConnected) {
      try {
        await remoteDataSource.signIn(phoneOrUsername, password);
        return const Right(unit);
      } on ServerException {
        return Left(ServerFailure());
      } on ErrorAuthException {
        return Left(ErrorAuthFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
