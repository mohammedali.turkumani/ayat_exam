import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/strings/failures.dart';
import '../../domain/usecases/sign_in.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final SignInUseCase signIn;

  AuthBloc({required this.signIn}) : super(AuthInitial()) {
    on<AuthEvent>(
      (event, emit) async {
        if (event is SignInEvent) {
          emit(LoadingSignInState());
          final failureOrSignIn = await signIn(
            phoneOrUsername: event.phoneOrUsername,
            password: event.password,
          );
          emit(_mapFailureOrSignInState(failureOrSignIn));
        }
      },
    );
  }

  AuthState _mapFailureOrSignInState(Either<Failure, Unit> either) {
    return either.fold(
      (failure) => ErrorAuthState(message: mapFailureToMessage(failure)),
      (unit) => const SuccessfullySignInState(),
    );
  }
}
