part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class SignInEvent extends AuthEvent {
  final String phoneOrUsername;
  final String password;

  const SignInEvent({required this.phoneOrUsername, required this.password});

  @override
  List<Object> get props => [phoneOrUsername, password];
}
