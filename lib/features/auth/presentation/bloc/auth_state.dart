part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();
}

class AuthInitial extends AuthState {
  @override
  List<Object> get props => [];
}

class LoadingSignInState extends AuthState {
  @override
  List<Object> get props => [];
}

class SuccessfullySignInState extends AuthState {
  const SuccessfullySignInState();

  @override
  List<Object> get props => [];
}

class ErrorAuthState extends AuthState {
  final String message;

  const ErrorAuthState({required this.message});

  @override
  List<Object> get props => [message];
}
