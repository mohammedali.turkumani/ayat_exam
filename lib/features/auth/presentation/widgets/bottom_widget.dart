import 'package:ayat_test/core/utils/extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/strings/app_strings.dart';
import '../../../../core/utils/custom_text_style.dart';
import 'bottom_shape.dart';

class BottomWidget extends StatelessWidget {
  const BottomWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomLeft,
      children: [
        CustomPaint(
          size: Size(1.sw, 1.sh),
          painter: BottomShape(),
        ),
        Positioned(
          left: 15.h,
          right: 0,
          bottom: 0,
          top: 0.84.sh,
          child: Text(
            AppString.notHaveAnaAccount,
            style: TextStyleApp.white14,
          ),
        ),
        Positioned(
          left: 0.08.sh,
          right: 0,
          bottom: 0,
          top: 0.76.sh,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.arrow_upward_outlined,
                color: Colors.white,
                size: 35.r,
              ),
              Text(
                AppString.register,
                style: TextStyleApp.white22UnderLine,
              ),
            ].withSpaceBetween(height: 0.01.sh),
          ),
        ),
        Positioned(
          left: 0.32.sh,
          right: 0,
          bottom: 0,
          top: 0.76.sh,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.person,
                color: Colors.blue,
                size: 35.r,
              ),
              Text(
                AppString.guest,
                style: TextStyleApp.blue22UnderLine,
              ),
            ].withSpaceBetween(height: 0.01.sh),
          ),
        ),
      ],
    );
  }
}
