import 'package:flutter/material.dart';

import '../../../../core/utils/colors.dart';

class BottomShape extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint0 = Paint()
      ..color = AppColors.originBlue
      ..style = PaintingStyle.fill
      ..strokeWidth = 1.0;

    Path path0 = Path();
    path0.moveTo(0, size.height * 0.9287588);
    path0.lineTo(size.width * 0.4644651, size.height * 0.9283552);
    path0.quadraticBezierTo(size.width * 0.4610465, size.height * 0.8564203,
        size.width * 0.5826512, size.height * 0.8560923);
    path0.quadraticBezierTo(size.width * 0.6931860, size.height * 0.8565464,
        size.width * 0.7071628, size.height * 0.9262361);
    path0.quadraticBezierTo(size.width * 0.7245349, size.height * 0.9835520,
        size.width * 0.8387907, size.height * 0.9844601);
    path0.quadraticBezierTo(size.width * 0.9564651, size.height * 0.9836907,
        size.width * 0.9753721, size.height * 0.9282669);
    path0.lineTo(size.width, size.height * 0.9282291);
    path0.lineTo(size.width, size.height * 1.0012614);
    path0.lineTo(0, size.height * 1.0018920);
    path0.lineTo(0, size.height * 0.9287588);
    path0.close();

    canvas.drawPath(path0, paint0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
