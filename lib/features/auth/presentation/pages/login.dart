import 'package:ayat_test/core/strings/assets.dart';
import 'package:ayat_test/core/utils/colors.dart';
import 'package:ayat_test/core/utils/custom_text_style.dart';
import 'package:ayat_test/core/utils/extensions.dart';
import 'package:ayat_test/core/utils/navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../core/strings/app_strings.dart';
import '../../../../core/utils/snackbar_message.dart';
import '../../../../core/widgets/custom_filled_button.dart';
import '../../../../core/widgets/custom_text_filled.dart';
import '../../../services/presentation/pages/services_screen.dart';
import '../bloc/auth_bloc.dart';
import '../widgets/bottom_widget.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);
  TextEditingController userNameOrMobileNumberController =
      TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.originWhite,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: AppColors.originWhite,
        elevation: 0.0,
        title: Text(
          AppString.login,
          style: TextStyleApp.blue30,
        ),
      ),
      body: Stack(
        children: [
          const BottomWidget(),
          Positioned.fill(
            bottom: 0.3.sh,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 0.12.sw),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SvgPicture.asset(Assets.domeLogo),
                  SizedBox(height: 20.h),
                  CustomTextField(
                    hintText: AppString.userNameHint,
                    controller: userNameOrMobileNumberController,
                  ),
                  CustomTextField(
                    hintText: AppString.passwordHint,
                    isUserName: false,
                    controller: passwordController,
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: TextButton(
                      onPressed: () {},
                      child: Text(
                        AppString.forgetPassword,
                        style: TextStyleApp.blue16,
                      ),
                    ),
                  ),
                  BlocConsumer<AuthBloc, AuthState>(
                    listener: (context, state) {
                      if (state is ErrorAuthState) {
                        SnackBarMessage().showErrorSnackBar(
                            message: state.message, context: context);
                      } else if (state is SuccessfullySignInState) {
                        navigateAndFinish(context, const ServicesScreen());
                      }
                    },
                    builder: (context, state) {
                      if (state is LoadingSignInState) {
                        return CustomFilledButton(
                          text: AppString.login,
                          isLoading: true,
                        );
                      }
                      return CustomFilledButton(
                        text: AppString.login,
                        onClick: () {
                          BlocProvider.of<AuthBloc>(context).add(
                            SignInEvent(
                              phoneOrUsername:
                                  userNameOrMobileNumberController.text,
                              password: passwordController.text,
                            ),
                          );
                        },
                      );
                    },
                  )
                ].withSpaceBetween(height: 20.h),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
