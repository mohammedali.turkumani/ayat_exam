import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../repositories/auth_repository.dart';

class SignInUseCase {
  final AuthRepository repository;

  SignInUseCase({required this.repository});

  Future<Either<Failure, Unit>> call(
      {required String phoneOrUsername, required String password}) async {
    return await repository.signIn(phoneOrUsername, password);
  }
}
