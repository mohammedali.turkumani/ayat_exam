import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';

abstract class AuthRepository {
  Future<Either<Failure, Unit>> signIn(String phoneOrUsername, String password);
}
