part of 'services_bloc.dart';

abstract class ServicesEvent extends Equatable {
  const ServicesEvent();
}

class GetMedicalServices extends ServicesEvent {
  @override
  List<Object> get props => [];
}
