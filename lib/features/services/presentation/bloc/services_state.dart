part of 'services_bloc.dart';

abstract class ServicesState extends Equatable {
  const ServicesState();
}

class ServicesInitial extends ServicesState {
  @override
  List<Object> get props => [];
}

class LoadingServicesState extends ServicesState {
  @override
  List<Object> get props => [];
}

class LoadedServicesState extends ServicesState {
  final List<MedicalService> medicalService;

  const LoadedServicesState({required this.medicalService});

  @override
  List<Object> get props => [medicalService];
}

class ErrorLoadingServicesState extends ServicesState {
  final String message;

  const ErrorLoadingServicesState({required this.message});

  @override
  List<Object> get props => [message];
}
