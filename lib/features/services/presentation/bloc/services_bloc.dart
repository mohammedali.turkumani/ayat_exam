import 'package:ayat_test/features/services/domain/entities/medical_service.dart';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../core/error/failure.dart';
import '../../../../core/strings/failures.dart';
import '../../domain/usecases/get_serives_user_case.dart';

part 'services_event.dart';
part 'services_state.dart';

class ServicesBloc extends Bloc<ServicesEvent, ServicesState> {
  final GetMedicalServicesUseCase getMedicalServicesUseCase;

  ServicesBloc({required this.getMedicalServicesUseCase})
      : super(ServicesInitial()) {
    on<ServicesEvent>((event, emit) async {
      if (event is GetMedicalServices) {
        emit(LoadingServicesState());
        final failureOrDoneOrders = await getMedicalServicesUseCase();
        emit(_mapFailureOrMedicalServicesToState(failureOrDoneOrders));
      }
    });
  }

  ServicesState _mapFailureOrMedicalServicesToState(
      Either<Failure, List<MedicalService>> either) {
    return either.fold(
      (failure) =>
          ErrorLoadingServicesState(message: mapFailureToMessage(failure)),
      (medicalService) => LoadedServicesState(medicalService: medicalService),
    );
  }
}
