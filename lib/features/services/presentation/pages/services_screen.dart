import 'package:ayat_test/core/strings/app_strings.dart';
import 'package:ayat_test/core/utils/colors.dart';
import 'package:ayat_test/core/utils/custom_text_style.dart';
import 'package:ayat_test/core/utils/extensions.dart';
import 'package:ayat_test/core/utils/navigator.dart';
import 'package:ayat_test/injection_container.dart' as di;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/utils/helper.dart';
import '../../../../core/utils/snackbar_message.dart';
import '../../../../core/widgets/custom_filled_button.dart';
import '../../domain/entities/medical_service.dart';
import '../bloc/services_bloc.dart';
import '../widgets/appbar_widget.dart';
import '../widgets/custom_service_card.dart';
import 'change_services_screen.dart';

class ServicesScreen extends StatelessWidget {
  final List<MedicalService>? medicalList;
  const ServicesScreen({Key? key, this.medicalList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.originWhite,
      body: SingleChildScrollView(
        child: Column(
          children: [
            const AppBarWidget(),
            BlocProvider(
              create: (context) =>
                  di.sl<ServicesBloc>()..add(GetMedicalServices()),
              child: BlocConsumer<ServicesBloc, ServicesState>(
                listener: (context, state) {
                  if (state is ErrorLoadingServicesState) {
                    SnackBarMessage().showErrorSnackBar(
                        message: state.message, context: context);
                  }
                },
                builder: (context, state) {
                  if (state is LoadingServicesState) {
                    return const Center(
                        child: CircularProgressIndicator(
                      color: AppColors.originBlue,
                    ));
                  } else if (state is LoadedServicesState) {
                    var medicalService = medicalList ?? state.medicalService;
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 30.w),
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.topRight,
                            child: CustomFilledButton(
                              text: AppString.myTasks,
                              heightSize: 0.04.sh,
                              radiusSize: 10.r,
                              widthSize: 0.4.sw,
                              onClick: () {},
                            ),
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  AppString.services,
                                  style: TextStyleApp.black25,
                                ),
                                Container(
                                  width: 0.12.sw,
                                  height: 0.005.sh,
                                  decoration: BoxDecoration(
                                    color: AppColors.originBlue,
                                    borderRadius: BorderRadius.horizontal(
                                      left: Radius.circular(5.r),
                                      right: Radius.circular(5.r),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Text(
                            AppString.facilityProviders,
                            style: TextStyleApp.grey18,
                          ),
                          ...medicalService.map(
                            (element) {
                              return element.isChecked!
                                  ? CustomServicesCard(
                                      title: Text(element.title,
                                          style: TextStyleApp.black20),
                                      isExpanded:
                                          element.subService.isNotEmpty &&
                                              hasCheckedSubService(element),
                                      child: element.subService.isEmpty
                                          ? null
                                          : Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                ...element.subService.map(
                                                  (subElement) {
                                                    if (subElement.isChecked!) {
                                                      return Text(
                                                        subElement.title,
                                                        style: TextStyleApp
                                                            .black16,
                                                      );
                                                    } else {
                                                      return const SizedBox
                                                          .shrink();
                                                    }
                                                  },
                                                ).toList()
                                              ].withSpaceBetween(height: 10.h),
                                            ),
                                    )
                                  : const SizedBox.shrink();
                            },
                          ).toList(),
                          CustomFilledButton(
                            widthSize: 1.sw,
                            text: AppString.addNewService,
                            onClick: () {
                              navigateTo(
                                context,
                                ChangeServiceScreen(
                                  medicalList: medicalService,
                                ),
                              );
                            },
                          )
                        ].withSpaceBetween(height: 15.w),
                      ),
                    );
                  }
                  return Container();
                },
              ),
            ),
          ].withSpaceBetween(height: 25.h),
        ),
      ),
    );
  }
}
