import 'package:ayat_test/core/strings/app_strings.dart';
import 'package:ayat_test/core/utils/colors.dart';
import 'package:ayat_test/core/utils/extensions.dart';
import 'package:ayat_test/core/utils/navigator.dart';
import 'package:ayat_test/features/services/presentation/pages/services_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/utils/custom_text_style.dart';
import '../../../../core/utils/helper.dart';
import '../../../../core/widgets/custom_filled_button.dart';
import '../../domain/entities/medical_service.dart';
import '../widgets/add_service_app_bar.dart';
import '../widgets/custom_check_box.dart';
import '../widgets/custom_service_card.dart';

class ChangeServiceScreen extends StatefulWidget {
  final List<MedicalService> medicalList;

  const ChangeServiceScreen({Key? key, required this.medicalList})
      : super(key: key);

  @override
  State<ChangeServiceScreen> createState() => _ChangeServiceScreenState();
}

class _ChangeServiceScreenState extends State<ChangeServiceScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.originWhite,
      body: SingleChildScrollView(
        child: Column(
          children: [
            AddServiceAppBar(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 30.w, vertical: 40.h),
              child: Column(
                children: [
                  ...widget.medicalList.map(
                    (element) {
                      return CustomServicesCard(
                        title: Row(
                          children: [
                            if (element.subService.isEmpty)
                              CustomCheckbox(
                                value: element.isChecked!,
                                onClick: () {
                                  setState(() {
                                    element.isChecked = !element.isChecked!;
                                  });
                                },
                              ),
                            Text(element.title, style: TextStyleApp.black20),
                          ].withSpaceBetween(width: 20.w),
                        ),
                        isExpanded: element.subService.isNotEmpty,
                        child: element.subService.isEmpty
                            ? null
                            : Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  ...element.subService
                                      .map(
                                        (subElement) => Row(
                                          children: [
                                            CustomCheckbox(
                                              value: subElement.isChecked!,
                                              onClick: () {
                                                setState(
                                                  () {
                                                    subElement.isChecked =
                                                        !subElement.isChecked!;

                                                    /// here for auto make the high level is false because
                                                    /// all low level is un check
                                                    if (!hasCheckedSubService(
                                                        element)) {
                                                      element.isChecked = false;
                                                    } else {
                                                      element.isChecked = true;
                                                    }
                                                  },
                                                );
                                              },
                                            ),
                                            Text(
                                              subElement.title,
                                              style: TextStyleApp.black16,
                                            ),
                                          ].withSpaceBetween(width: 20.w),
                                        ),
                                      )
                                      .toList()
                                ].withSpaceBetween(height: 10.h),
                              ),
                      );
                    },
                  ).toList(),
                  CustomFilledButton(
                    widthSize: 1.sw,
                    text: AppString.done,
                    onClick: () {
                      navigateAndReplacement(context,
                          ServicesScreen(medicalList: widget.medicalList));
                    },
                  )
                ].withSpaceBetween(height: 15.w),
              ),
            )
          ],
        ),
      ),
    );
  }
}
