import 'package:ayat_test/core/strings/app_strings.dart';
import 'package:ayat_test/core/utils/extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/utils/colors.dart';
import '../../../../core/utils/custom_text_style.dart';

class AppBarWidget extends StatelessWidget {
  const AppBarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1.sw,
      height: 0.18.sh,
      decoration: BoxDecoration(
        color: AppColors.originBlue,
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(50.r),
          bottomLeft: Radius.circular(50.r),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 30.w,
          vertical: 40.h,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Expanded(
                  flex: 4,
                  child: Row(
                    children: [
                      Flexible(
                        flex: 1,
                        child: Text(
                          AppString.servicesAppBar,
                          style: TextStyleApp.white24Bold,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          width: 50.w,
                          height: 50.h,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(color: Colors.white, width: 5),
                          ),
                          child: Center(
                            child: Text(
                              AppString.oneHundred,
                              style: TextStyleApp.white12,
                            ),
                          ),
                        ),
                      ),
                    ].withSpaceBetween(width: 10.w),
                  ),
                ),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.message,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Expanded(
                        child: IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.keyboard_arrow_right,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ].withSpaceBetween(width: 20.w),
                  ),
                )
              ],
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 0.16.sw),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Icon(
                      Icons.person,
                      color: Colors.white,
                    ),
                    Icon(
                      Icons.cell_tower,
                      color: Colors.white,
                    ),
                    Icon(
                      Icons.clean_hands_outlined,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            )
          ].withSpaceBetween(height: 15.h),
        ),
      ),
    );
  }
}
