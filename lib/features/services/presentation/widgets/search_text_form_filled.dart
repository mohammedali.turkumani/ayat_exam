import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/utils/colors.dart';
import '../../../../core/utils/custom_text_style.dart';

class SearchFormFilled extends StatelessWidget {
  final TextEditingController controller;
  const SearchFormFilled({Key? key, required this.controller})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 0.15.sh,
      right: 0.1.sw,
      child: SizedBox(
        width: 0.8.sw,
        height: 0.045.sh,
        child: Center(
          child: TextFormField(
            controller: controller,
            style: TextStyleApp.black14,
            decoration: InputDecoration(
              contentPadding:
                  EdgeInsets.symmetric(vertical: 9.h, horizontal: 10.w),
              isDense: true,
              fillColor: Colors.white,
              filled: true,
              suffixIcon: const Icon(Icons.close, color: Colors.grey),
              prefixIcon: const Icon(Icons.search, color: AppColors.originBlue),
              border: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.grey),
                borderRadius: BorderRadius.circular(20.r),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: AppColors.originBlue),
                borderRadius: BorderRadius.circular(20.r),
              ),
              hintStyle: TextStyleApp.grey14,
              hintText: "Search",
              errorStyle: TextStyleApp.errorStyle,
              enabledBorder: OutlineInputBorder(
                borderSide:
                    BorderSide(color: Colors.grey.withOpacity(0.4), width: 1.0),
                borderRadius: BorderRadius.circular(20.r),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.red),
                borderRadius: BorderRadius.circular(20.r),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
