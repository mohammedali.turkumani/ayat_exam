import 'package:ayat_test/features/services/presentation/widgets/search_text_form_filled.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/utils/colors.dart';
import '../../../../core/utils/custom_text_style.dart';

class AddServiceAppBar extends StatelessWidget {
  AddServiceAppBar({Key? key}) : super(key: key);
  TextEditingController searchController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 1.sw,
      height: 0.2.sh,
      child: Stack(
        children: [
          Column(
            children: [
              Container(
                width: 1.sw,
                height: 0.18.sh,
                decoration: BoxDecoration(
                  color: AppColors.originBlue,
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(90.r),
                    bottomLeft: Radius.circular(90.r),
                  ),
                ),
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 30.w, vertical: 40.h),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          flex: 3,
                          child: Text(
                            "Add new services",
                            style: TextStyleApp.white28,
                          ),
                        ),
                        Flexible(
                          flex: 1,
                          child: IconButton(
                            onPressed: () {},
                            icon: const Icon(
                              Icons.keyboard_arrow_right,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          SearchFormFilled(controller: searchController)
        ],
      ),
    );
  }
}
