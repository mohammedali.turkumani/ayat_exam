import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../../core/utils/colors.dart';

class CustomCheckbox extends StatefulWidget {
  final void Function() onClick;
  final bool value;
  const CustomCheckbox({Key? key, required this.value, required this.onClick})
      : super(key: key);

  @override
  State<CustomCheckbox> createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onClick,
      child: AnimatedContainer(
        height: 28,
        width: 28,
        duration: const Duration(milliseconds: 500),
        curve: Curves.fastLinearToSlowEaseIn,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: AppColors.originBlue.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 12,
              offset: const Offset(0, 0), // changes position of shadow
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.r),
          border: Border.all(
            color: widget.value
                ? AppColors.originBlue
                : AppColors.originBlue.withOpacity(0.3),
            width: 2,
          ),
        ),
        child: widget.value
            ? Padding(
                padding: const EdgeInsets.all(5.0),
                child: Container(
                  width: 2.w,
                  height: 2.h,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: AppColors.originBlue,
                  ),
                ),
              )
            : null,
      ),
    );
  }
}
