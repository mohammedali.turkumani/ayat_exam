import 'package:ayat_test/core/utils/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'expanded_widget.dart';

class CustomServicesCard extends StatefulWidget {
  final Widget? child;
  final Widget title;
  final bool animatePadding;
  final bool? isExpanded;

  const CustomServicesCard({
    Key? key,
    this.child,
    required this.title,
    this.animatePadding = true,
    this.isExpanded = false,
  }) : super(key: key);
  @override
  CustomServicesCardState createState() => CustomServicesCardState();
}

class CustomServicesCardState extends State<CustomServicesCard>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> animation;
  final Duration animationDuration = const Duration(milliseconds: 500);
  bool _expanded = false;

  bool get isExpanded => _expanded;

  @override
  void initState() {
    super.initState();
    initAnimations();
    _animate();
  }

  void collapse() {
    setState(() {
      _expanded = false;
    });
    _animate();
  }

  void expand() {
    setState(() {
      _expanded = true;
    });
    _animate();
  }

  void toggle() {
    setState(() {
      _expanded = !_expanded;
    });
    _animate();
  }

  @override
  void didUpdateWidget(covariant CustomServicesCard oldWidget) {
    super.didUpdateWidget(oldWidget);
    _animate();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void initAnimations() {
    _controller = AnimationController(
      vsync: this,
      duration: animationDuration,
    );
    animation = Tween<double>(begin: 0, end: -0.25).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.fastOutSlowIn,
      ),
    );
  }

  void _animate() {
    if (_expanded) {
      _controller.forward();
    } else {
      _controller.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.isExpanded! ? toggle : null,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 10.h),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          color: Colors.white,
          gradient: _expanded
              ? const LinearGradient(
                  begin: Alignment.centerLeft,
                  end: Alignment.centerRight,
                  stops: [0.03, 0.03],
                  colors: [AppColors.originBlue, Colors.white],
                )
              : null,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              blurRadius: 10.0,
              spreadRadius: 5.0,
            ),
          ],
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 10,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: widget.title,
                  ),
                  Visibility(
                    visible: widget.isExpanded!,
                    child: RotationTransition(
                      turns: animation,
                      child: CircleAvatar(
                        backgroundColor: AppColors.originBlue,
                        maxRadius: 12,
                        child: Icon(
                          _expanded
                              ? Icons.keyboard_arrow_right_sharp
                              : Icons.keyboard_arrow_down_sharp,
                          size: 20,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (widget.isExpanded!)
              ExpandedWidget(
                expand: _expanded,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 10,
                  ),
                  child: widget.child,
                ),
              ),
          ],
        ),
      ),
    );
  }
}
