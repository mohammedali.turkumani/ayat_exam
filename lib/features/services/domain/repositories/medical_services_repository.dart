import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entities/medical_service.dart';

abstract class MedicalServicesRepository {
  Future<Either<Failure, List<MedicalService>>> getServices();
}
