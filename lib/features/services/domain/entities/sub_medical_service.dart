import 'package:equatable/equatable.dart';

class SubMedicalService extends Equatable {
  final String serviceId;
  final String title;
  bool? isChecked;

  SubMedicalService({
    required this.serviceId,
    required this.title,
    this.isChecked,
  });

  // Add a custom setter method for isChecked
  void setChecked(bool value) {
    isChecked = value;
  }

  @override
  List<Object?> get props => [serviceId, title, isChecked];
}
