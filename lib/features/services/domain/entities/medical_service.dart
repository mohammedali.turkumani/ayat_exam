import 'package:ayat_test/features/services/domain/entities/sub_medical_service.dart';
import 'package:equatable/equatable.dart';

class MedicalService extends Equatable {
  final String serviceId;
  final String title;
  bool? isChecked;

  final List<SubMedicalService> subService;
// Add a custom setter method for isChecked
  void setChecked(bool value) {
    isChecked = value;
  }

  MedicalService({
    required this.serviceId,
    required this.title,
    this.isChecked,
    required this.subService,
  });

  @override
  List<Object?> get props => [serviceId, title, isChecked, subService];
}
