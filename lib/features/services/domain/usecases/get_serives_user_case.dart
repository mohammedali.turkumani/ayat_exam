import 'package:dartz/dartz.dart';

import '../../../../core/error/failure.dart';
import '../entities/medical_service.dart';
import '../repositories/medical_services_repository.dart';

class GetMedicalServicesUseCase {
  final MedicalServicesRepository repository;

  GetMedicalServicesUseCase({required this.repository});

  Future<Either<Failure, List<MedicalService>>> call() async {
    return await repository.getServices();
  }
}
