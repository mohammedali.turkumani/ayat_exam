import 'package:dartz/dartz.dart';

import '../../../../core/error/exceptions.dart';
import '../../../../core/error/failure.dart';
import '../../../../core/network/network_info.dart';
import '../../domain/entities/medical_service.dart';
import '../../domain/repositories/medical_services_repository.dart';
import '../datasources/remote_data_source.dart';

class MedicalServicesRepositoryImpl implements MedicalServicesRepository {
  final ServicesRemoteDataSource servicesRemoteDataSource;
  final NetworkInfo networkInfo;

  MedicalServicesRepositoryImpl({
    required this.servicesRemoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<MedicalService>>> getServices() async {
    if (await networkInfo.isConnected) {
      try {
        List<MedicalService> services =
            await servicesRemoteDataSource.getServices();
        return Right(services);
      } on ServerException {
        return Left(ServerFailure());
      } on ErrorAuthException {
        return Left(ErrorAuthFailure());
      }
    } else {
      return Left(OfflineFailure());
    }
  }
}
