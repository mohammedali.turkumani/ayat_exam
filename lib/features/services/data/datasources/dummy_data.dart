import '../models/service_model.dart';
import '../models/sub_medical_service_model.dart';

List<MedicalServiceModel> getDummyMedicalServices() {
  return [
    MedicalServiceModel(
      serviceId: '1',
      title: 'General Checkup',
      isChecked: true,
      subService: [
        SubMedicalServiceModel(
          serviceId: '1_1',
          title: 'Blood Pressure Check',
          isChecked: true,
        ),
        SubMedicalServiceModel(
          serviceId: '1_2',
          title: 'Temperature Measurement',
          isChecked: false,
        ),
      ],
    ),
    MedicalServiceModel(
      serviceId: '2',
      title: 'Dental Services',
      isChecked: true,
      subService: [
        SubMedicalServiceModel(
          serviceId: '1_1',
          title: 'Blood Pressure ',
          isChecked: false,
        ),
        SubMedicalServiceModel(
          serviceId: '1_2',
          title: 'Temperature Measurement',
          isChecked: false,
        ),
      ],
    ),
    MedicalServiceModel(
      serviceId: '5',
      title: 'Lab Tests',
      isChecked: true,
      subService: [
        SubMedicalServiceModel(
          serviceId: '5_1',
          title: 'Complete Blood Count (CBC)',
          isChecked: false,
        ),
        SubMedicalServiceModel(
          serviceId: '5_2',
          title: 'Urinalysis',
          isChecked: true,
        ),
        SubMedicalServiceModel(
          serviceId: '5_3',
          title: 'Glucose Test',
          isChecked: true,
        ),
        SubMedicalServiceModel(
          serviceId: '5_4',
          title: 'Lipid Profile',
          isChecked: false,
        ),
      ],
    ),
    MedicalServiceModel(
      serviceId: '6',
      title: 'Physical Therapy',
      isChecked: true,
      subService: [
        SubMedicalServiceModel(
          serviceId: '6_1',
          title: 'Therapeutic Exercises',
          isChecked: true,
        ),
        SubMedicalServiceModel(
          serviceId: '6_2',
          title: 'Heat Therapy',
          isChecked: false,
        ),
        SubMedicalServiceModel(
          serviceId: '6_3',
          title: 'Massage Therapy',
          isChecked: false,
        ),
      ],
    ),
    MedicalServiceModel(
      serviceId: '3',
      title: 'Vaccinations',
      isChecked: false,
      subService: [],
    ),
    MedicalServiceModel(
      serviceId: '4',
      title: 'X-ray and Imaging',
      isChecked: true,
      subService: [],
    ),
    // Add more sample data here if needed
  ];
}
