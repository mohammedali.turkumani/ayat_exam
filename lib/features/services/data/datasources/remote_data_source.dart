import '../models/service_model.dart';
import 'dummy_data.dart';

abstract class ServicesRemoteDataSource {
  Future<List<MedicalServiceModel>> getServices();
}

class ServicesRemoteDataSourceImp implements ServicesRemoteDataSource {
  ServicesRemoteDataSourceImp();

  @override
  Future<List<MedicalServiceModel>> getServices() async {
    await Future.delayed(const Duration(seconds: 1));

    List<MedicalServiceModel> medicalServices = getDummyMedicalServices();

    return medicalServices;
  }
}
