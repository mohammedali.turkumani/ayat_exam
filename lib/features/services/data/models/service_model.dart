import '../../domain/entities/medical_service.dart';
import '../../domain/entities/sub_medical_service.dart';

class MedicalServiceModel extends MedicalService {
  MedicalServiceModel(
      {required String serviceId,
      required String title,
      required bool? isChecked,
      required List<SubMedicalService> subService})
      : super(
            serviceId: serviceId,
            title: title,
            isChecked: isChecked,
            subService: subService);

  factory MedicalServiceModel.fromJson(Map<String, dynamic> json) {
    return MedicalServiceModel(
      serviceId: json['serviceId'],
      title: json['title'],
      isChecked: json['isChecked'],
      subService: List<SubMedicalService>.from(
        json["subService"].map((subService) => subService),
      ),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'serviceId': serviceId,
      'title': title,
      'isChecked': isChecked,
      "subService": subService,
    };
  }
}
