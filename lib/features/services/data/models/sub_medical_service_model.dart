import '../../domain/entities/sub_medical_service.dart';

class SubMedicalServiceModel extends SubMedicalService {
  SubMedicalServiceModel({
    required String serviceId,
    required String title,
    required bool isChecked,
  }) : super(serviceId: serviceId, title: title, isChecked: isChecked);

  factory SubMedicalServiceModel.fromJson(Map<String, dynamic> json) {
    return SubMedicalServiceModel(
      serviceId: json['serviceId'],
      title: json['title'],
      isChecked: json['isChecked'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'serviceId': serviceId,
      'title': title,
      'isChecked': isChecked,
    };
  }
}
